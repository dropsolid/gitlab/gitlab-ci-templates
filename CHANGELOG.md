This file will list the changes requiring actions in your setup if you update from one tag to another.

## 0.3.0

The `code-quality:phpcs` job can now be configured to check your codebase in full, or in incremental mode.

Full is now the default behavior. If you want to keep on using the incremetal mode, please set
`PHPCS_INCREMENTAL_CHECK` to "1".

In order to have the full mode working, it is required to have a `phpcs.xml` file configured at the root of the project.
Incremental mode does not require it, but it is highly recommended to set it up.
See https://dropsolid.atlassian.net/wiki/spaces/GQA/pages/760905821/PHPCS+configuration

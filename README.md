# Gitlab CI/CD default template

See https://git.drupalcode.org/project/gitlab_templates for the original templates and their documentation

## Getting started
Start a new `feature/gitlab-ci` branch (or any branch who's name starts with `feature/gitlab-ci`).

Copy the [.gitlab-ci.yml template file](gitlab-ci/template.gitlab-ci.yml) in your project repository, and rename it to
`.gitlab-ci.yml`. Then head over to [includes/gitlab-ci.variables.yml](includes/gitlab-ci.variables.yml) to see the
available variables, and override the one that are specific to your project in the gitlab-ci.yml file on your project (eg `PHP_VERSION`, `NODE_VERSION`,
`THEME_NAME` etc).

In this template, `ref` is set to use the `master` branch of this repo, meaning your pipelines will always reflect the latest state
of this template, which may not be the expected behavior, as it could potentially break your pipelines if you customized them a bit.
The recommended approach is to use `master` while setting up gitlab-ci in your project, then stick to a [tagged release](https://gitlab.dropsolid.com/project/gitlabtemplates/-/tags) once you are done.

Also, see the [examples file](gitlab-ci/examples.gitlab-ci.yml) for examples on how to extend the file.

Once done, commit and push your new file and go to the CI/CD > Pipeline section of your project's Gitlab repository,
and watch your pipeline run (and probably fail).

Once happy with the results, it can be merged into the other integration branches.

The last file to have a look at is [the workflow definition file](includes/gitlab-ci.workflow.yml), which is responsible
to define _when_ a pipeline should be created. In short, and by default, pipeline will run on merge requests and on
`develop`, `staging` and `master` branches. Additionally, it will run for all branches starting with
`feature/gitlab-ci`, which is useful to avoid polluting your history while playing with the .gitlab-ci.yml file.

## Known issues
### HTTP 413 - Content Too Large
build:npm can fail due to the generated artifacts being too large (413 - Content Too Large). To mitigate this, infra
raised the maximum artifacts size to 1GB. However, for some projects it might not suffice.
To fix it on your end while a reliable solution is being discussed, you can implement the following change in your
gitlab-ci.yml file. It will have as effect to run `npm ci` before running the linters.
e.g:
```yaml
build:npm:
  artifacts: null

code-quality:js-lint:
  before_script:
    - !reference [.theme_js_lint, before_script]
    - npm ci

code-quality:css-lint:
  before_script:
    - !reference [.theme_css_lint, before_script]
    - npm ci
```

To be repeated if you have defined other linter jobs for secondary themes.

## Jobs
The following jobs are configured to run by default

### build:composer
Used to fetch all required composer packages, including the dev ones.

### build:npm
Used to fetch all required node packages.

### security:composer-audit
Will check your packages for known vulnerability issues.

Can be skipped by setting the `SKIP_COMPOSER_AUDIT` variable to "1"

### code-quality:phpstan
Run PHPStan on your codebase. This job will be skipped if there is no `phpstan.neon` file at the root of your project.

Can be skipped by setting the `SKIP_PHPSTAN` variable to "1"

### code-quality:phpcs
Run PHPCS on your code base, using the phpcs.xml(.dist) configuration in the project. By default, this will check for all files.

It is possible to configure it to perform incremental checks by setting the `PHPCS_INCREMENTAL_CHECK` variable to "1".
In incremental mode, it will check for violations between the current branch and the branch (or tag) configured in the `CHANGES_COMPARISON_BRANCH` variable (default to master).

Can be skipped by setting the `SKIP_PHPCS` variable to "1"

### code-quality:js-lint
Lint your javascript files by invoking the command defined in the `JS_LINT_COMMAND` variable. Default to
`js:lint:precommit`. You'll probably need to change this for older projects.

Can be skipped by setting the `SKIP_JS_LINT` variable to "1"

### code-quality:css-lint
Lint your css files by invoking the command defined in the `CSS_LINT_COMMAND` variable. Default to
`css:lint:precommit`. You'll probably need to change this for older projects.

Can be skipped by setting the `SKIP_CSS_LINT` variable to "1"

#### Note for linter jobs
If your project contains more than one theme, check in [gitlab-ci/examples.gitlab-ci.yml](gitlab-ci/examples.gitlab-ci.yml) how to declare new jobs
to provide linting in theme.

### test:phpunit:unit, test:phpunit:kernel, test:phpunit:functional, test:phpunit:functional-js
Run php unit test that may live in your custom code base. There are four jobs, one per testsuite. The name of the
test suites to run can be configured in `PHPUNIT_<TEST_TYPE>_TESTSUITE` variables. These values should be set to the
suites defined in you `phpunit.xml` file. This job will be skipped if there is no `phpunit.xml` file at the root of your
project.

PHP unit jobs are disabled by default as only few projects are using it, and no company level agreement has been made on
its usage yet. You can turn it on by setting `SKIP_PHPUNIT` variable to 0. Then there are `SKIP_PHPUNIT_<TEST_TYPE>`
variables to turn on and off as well
